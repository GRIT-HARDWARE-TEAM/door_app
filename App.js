import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';

export default class App extends Component {
    state = {}
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    activeOpacity={0.3}
                    style={styles.button}
                    onPress={async () => {
                        try {
                            let key = "YjgzN2Q2OWIxYTI2NzQ4NzIyYjViODI3NDI"
                            
                            let resp = await fetch('http://192.168.0.108:9000/'+key)
                            let json = await resp.json();
                            let status = json["status"];
                            if (!status){
                                alert(json["message"])
                                return
                            }   
                            
                        } catch (error) {
                            alert(error.message)
                        }
                    }}>
                    <Text style={styles.info}>Open Sesame</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00b490',
    },
    button: {
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
        alignItems: 'center',
        height: 300,
        width: 200,
        borderRadius: 10,
    },
    info: {
        fontSize: 40,
        textAlign: 'center',
        margin: 10,
        color: 'black'
    },
});
